<?php

use Illuminate\Support\Facades\Route;
use App\Models\Resource;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    $resources = Resource::all();
    return view('welcome', ['resources'=>$resources]);
});

Route::post('/file-upload', [\App\Http\Controllers\FileUploadController::class, 'fileUpload'])->name('fileUpload');