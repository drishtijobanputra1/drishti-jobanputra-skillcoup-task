<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Resource;

class FileUploadController extends Controller
{
    public function fileUpload(Request $request){
        foreach($request->file('file') as $f){
            $resource = new Resource;
        
        $path = $f;
        $filename = $path->getClientOriginalName();//making a unique name
        $path->move('images/resource', $filename);//storing and then moving file to public folder
        $resource->file_path =$filename;//stoirng resource path
       
        $resource->save();
        }
    }
}
