<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>

        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>SkillCoup</title>

        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
        
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css">
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.min.js"></script>

        <script src="https://unpkg.com/dropzone@5/dist/min/dropzone.min.js"></script>
        <link rel="stylesheet" href="https://unpkg.com/dropzone@5/dist/min/dropzone.min.css" type="text/css" />
        
        <link rel="stylesheet" href="https://cdn.datatables.net/1.12.1/css/jquery.dataTables.min.css" type="text/css" />
        <script src="https://cdn.datatables.net/1.12.1/js/jquery.dataTables.min.js"></script>
        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap" rel="stylesheet">

        <style>
            body {
                font-family: 'Nunito', sans-serif;
            }
        </style>
    </head>
    <body>
        <div class="container-md mt-5">
            <form action="{{route('fileUpload')}}" class="dropzone" id="my-awesome-dropzone" method="POST" enctype="multipart/form-data">
            @csrf
        </form>
        <div class="card mt-5">
            <div class="card-header">
                <div class="card-title">
                    Resources
                </div>
            </div>
            <div class="card-body">
                <table id="displayTable" class="table table-bordered table-striped dataTable table-responsive w-100 d-block d-xxl-table">
                    <thead>
                        <tr>
                            <th>Sr No.</th>
                            <th>File</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($resources as $resource)
                            <tr>
                                <td>{{$loop->iteration}}</td>
                                <td><a href="{{asset('images/resource/'. $resource->file_path)}}" target="_BLANK">{{$resource->file_path}}</a></td>
                            </tr>      
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        </div>
        <script>
            // Note that the name "myDropzone" is the camelized
            // $('#my-awesome-dropzone')
            Dropzone.options.myAwesomeDropzone = {
                uploadMultiple: true,
                parallelUploads: 100,
                maxFilesize: 2,
                init: function() {
                var myDropzone = this;
                this.on("successmultiple", function(file) {
                alert('File Uploaded');
                
                location.reload()
            })
            }
            };
          </script>
          <script>
            $('#displayTable').DataTable();
          </script>
    </body>
</html>
